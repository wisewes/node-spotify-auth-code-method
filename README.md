## Spotify Web API Tutorial

This is a node.js project that accesses a user's Spotify data by using Spotify's access code authorization method.

The tutorial can be found at [https://developer.spotify.com/web-api/tutorial/](https://developer.spotify.com/web-api/tutorial/)

## Future Enhancements
* redesign UI
* create a feature-rich UI using a front-end framework
