// app.js

var express = require('express');
var request = require('request');
var queryString = require('querystring');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var logger = require('morgan');

var config = require('./auth/config');
var Auth = require('./auth/auth');

//express setup
var app = express();
app.use(express.static(__dirname + '/public'));
app.use(logger('dev'));
app.use(cookieParser());
app.use(session({
  secret: 'copertosteeltoahingethatisfaltered',
  resave: true,
  saveUninitialized: true
}));

//authorization flow
app.get('/login', Auth.login);
app.get('/callback', Auth.callback);
app.get('/refresh_token', Auth.refresh);

//start server
var server = app.listen(4000, function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Server is running on port ' + port);
});
