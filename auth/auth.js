// auth.js
var queryString = require('querystring');
var request = require('request');
var config = require('./config');

//helper
var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

//randomized string for state value
var state = generateRandomString(16);


module.exports = {

  login: function(req, res) {

    res.cookie(config.state_key, state);

    var params = {
      response_type: 'code',
      client_id: config.client_id,
      scope: config.scope,
      redirect_uri: config.redirect_uri,
      state: state
    };

    res.redirect('https://accounts.spotify.com/authorize?' +
      queryString.stringify(params));
  },

  callback: function(req, res) {

    var code = req.query.code || null;
    var state = req.query.state || null;
    var storedState = req.cookies ? req.cookies[config.state_key] : null;

    if (state === null || state !== storedState) {
        res.redirect('/#' +
          queryString.stringify({
            error: 'state_mismatch'
          }));
      } else {

        res.clearCookie(config.state_key);

        var authOptions = {
          url: 'https://accounts.spotify.com/api/token',
          form: {
            code: code,
            redirect_uri: config.redirect_uri,
            grant_type: 'authorization_code'
          },
          headers: {
            'Authorization': 'Basic ' + (new Buffer(config.client_id + ':' + config.client_secret).toString('base64'))
          },
          json: true
        };

        request.post(authOptions, function(error, response, body) {
          if (!error && response.statusCode === 200) {

            var access_token = body.access_token;
            var refresh_token = body.refresh_token;

            var options = {
              url: 'https://api.spotify.com/v1/me',
              headers: { 'Authorization': 'Bearer ' + access_token },
              json: true
            };

            // use the access token to access the Spotify Web API
            request.get(options, function(error, response, body) {
              console.log(body);
              req.session.access_token = access_token;
              req.session.refresh_token = refresh_token;
            });

            // we can also pass the token to the browser to make requests from there
            res.redirect('/#' +
              queryString.stringify({
                access_token: access_token,
                refresh_token: refresh_token
              }));

          } else {
            res.redirect('/#' +
              queryString.stringify({
                error: 'invalid_token'
              }));
          }
        });
      }
    },

    refresh: function(req, res) {

      // requesting access token from refresh token
      var refresh_token = req.query.refresh_token;
      var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        headers: { 'Authorization': 'Basic ' + (new Buffer(config.client_id + ':' + config.client_secret).toString('base64')) },
        form: {
          grant_type: 'refresh_token',
          refresh_token: refresh_token
        },
        json: true
      };

      request.post(authOptions, function(error, response, body) {

        if (!error && response.statusCode === 200) {
          var access_token = body.access_token;
          res.send({
            'access_token': access_token
          });
        }
      });
    }
};
